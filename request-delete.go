package main

import (
        "fmt"
        "log"
        "gopkg.in/mgo.v2"
        "strconv"
        //"gopkg.in/mgo.v2/bson"
)

type Request struct {
        AppKey string
        DeviceToken string
        Message string
        Status bool
}

func main() {
        //connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

		/*to insert multiple document using loop*/

        //convenient access
        c := session.DB("test").C("Requests")

        //delete
        err = c.Remove(bson.M{"message": "Req1Delete"})

        if err != nil {
                log.Fatal(err)
        }
        
}