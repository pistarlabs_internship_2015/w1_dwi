package main

import (
        "fmt"
	"log"
        "gopkg.in/mgo.v2"
        "gopkg.in/mgo.v2/bson"
)

type Request struct {
        AppKey string
        DeviceToken string
        Message string
        Status bool
}

func main() {
        //connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("test").C("Requests")
        err = c.Insert(&Request{"1a9b34cd", "1234567890", "Req1", true},
	                     &Request{"2b3c49de", "2345678901", "Req3", false},
                       &Request{"1a5b34cr", "3456789012", "Req1", true},
                       &Request{"2b3c45dh", "2345678901", "Req3", true},
                       &Request{"1a2b89cs", "1234567890", "Req3", true},
                       &Request{"2b3c56dq", "2345678901", "Req1", false},
                       &Request{"1a2b34cy", "3456789012", "Req3", true},
                       &Request{"2b3c76di", "3456789012", "Req2", false},
                       &Request{"1a2b90cp", "1234567890", "Req3", true},
                       &Request{"2b3c00dc", "2345678901", "Req1", true},
                       &Request{"3c4d23ev", "3456789012", "Req3", false})
        if err != nil {
                log.Fatal(err)
        }


        result := Request{}
        iter := c.Find(bson.M{"status": false}).Iter()

        for iter.Next(&result){
                fmt.Print("\n\nAppKey       : ", result.AppKey, "")
                fmt.Print("\nDeviceToken  : ", result.DeviceToken)
                fmt.Print("\nMessage      : ", result.Message)
        }
        
}