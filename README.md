 PISTARLABS INTERNSHIP 2015
 
 
 * MongoDB Manual
 * Working with MongoDB
 
 * request-insert-loop.go :
   insert data yang cukup banyak, pakai for untuk looping
 * request-insert-no-loop.go :
   insert data manual, tanpa looping
 * request-update.go :
   update field data sesuai dengan condition
 * request-delete.go :
   delete field data sesuai dengan condition
 * mongo-command.txt :
   command yang digunakan dalam mongoDB(create collection, insert, update, find data)
 
 
 --
 
 Dwi Putri Butarbutar
 Pistarlabs Internship Team 2015