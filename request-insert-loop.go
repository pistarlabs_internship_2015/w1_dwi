package main

import (
        "fmt"
		"log"
        "gopkg.in/mgo.v2"
        "strconv"
        //"gopkg.in/mgo.v2/bson"
)

type Request struct {
        AppKey string
        DeviceToken string
        Message string
        Status bool
}

func main() {
        //connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

		/*to insert multiple document using loop*/

        //convenient access
        c := session.DB("test").C("Requests")
        for i := 1; i <= 26; i++{
        	t := strconv.Itoa(i)
        	var app_key = "1f7k"+string(t)+"ew";
        	var dev_token = "1234567890";
        	var msg = "Req1";
        	var stat = true;
        	{
        		err = c.Insert(&Request{string(app_key), dev_token, msg, stat})
        	}
        	fmt.Println("Success #", i)
        }

        if err != nil {
                log.Fatal(err)
        }
        
}