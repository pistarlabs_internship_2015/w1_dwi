package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var jsonReq = []byte(`[
		{"Message": "Request1", "Status": true},
		{"Message": "Request2", "Status": false}
	]`)
	type Request struct {
		Message  string
		Status bool
	}
	var requests []Request
	err := json.Unmarshal(jsonReq, &requests)
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Printf("%+v", requests)
}