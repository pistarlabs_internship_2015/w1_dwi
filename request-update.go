package main

import (
        "fmt"
	"log"
        "gopkg.in/mgo.v2"
        "gopkg.in/mgo.v2/bson"
)

type Request struct {
        AppKey string
        DeviceToken string
        Message string
        Status bool
}

func main() {
        //connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("test").C("Requests")

        //update
        colQuerier := bson.M{"message": "Req3test"}
        change := bson.M{"$set": bson.M{"message": "Req3"}}
        err = c.Update(colQuerier, change)
        if err != nil {
                log.Fatal(err)
        }

        result := Request{}
        iter := c.Find(bson.M{"message": "Req3"}).Iter()

        for iter.Next(&result){
                fmt.Print("\n\nAppKey       : ", result.AppKey, "")
                fmt.Print("\nDeviceToken  : ", result.DeviceToken)
                fmt.Print("\nStatus     : ", result.Status)
        }
        
}