package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	type PistarlabsGroup struct {
		ID     int
		Name   string
		Jobs []string
	}
	group := PistarlabsGroup{
		ID:     1,
		Name:   "Internship",
		Jobs: []string{"Analyst", "Designer", "Programmer", "Tester"},
	}
	b, err := json.Marshal(group)
	if err != nil {
		fmt.Println("error:", err)
	}
	os.Stdout.Write(b)
}